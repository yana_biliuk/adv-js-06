const button = document.getElementById("button");
button.addEventListener("click", async () => {
  try {
    const response = await fetch("https://api.ipify.org/?format=json");
    const { ip } = await response.json();

    const responsIP = await fetch(`http://ip-api.com/json/${ip}`);
    const adressInfo = await responsIP.json();
    console.log(adressInfo);

    const { continent, country, regionName, city, district } = adressInfo;
    const div = document.getElementById("div");
    div.innerHTML = `
      <p><strong>Континент:</strong> ${continent}</p>
      <p><strong>Країна:</strong> ${country}</p>
      <p><strong>Регіон:</strong> ${regionName}</p>
      <p><strong>Місто:</strong> ${city}</p>
      <p><strong>Район:</strong> ${district}</p>
      `;
  } catch (error) {
    console.error("Сталася помилка:", error);
  }
});

// const button = document.getElementById("button");
// button.addEventListener("click", async () => {
//   try {
//     const ip = await findIP();
//     const adressInfo = await getAgressInfo(ip);
//     render(adressInfo);
//   } catch (error) {
//     console.log("Сталася помилка:", error);
//   }
// });

// async function findIP() {
//   try {
//     const response = await fetch("https://api.ipify.org/?format=json");
//     const { ip } = await response.json();
//     return ip;
//   } catch (error) {
//     throw error;
//   }
// }

// async function getAgressInfo(ip) {
//   try {
//     const response = await fetch(`http://ip-api.com/json/${ip}`);
//     const addressInfo = await response.json();

//     return addressInfo;
//   } catch (error) {
//     throw error;
//   }
// }

// function render(adressInfo) {
//   const { continent, country, regionName, city, district } = adressInfo;
//   const div = document.getElementById("div");
//   div.innerHTML = `
// <p><strong>Континент:</strong> ${continent}</p>
// <p><strong>Країна:</strong> ${country}</p>
// <p><strong>Регіон:</strong> ${regionName}</p>
// <p><strong>Місто:</strong> ${city}</p>
// <p><strong>Район:</strong> ${district}</p>
// `;
// }


